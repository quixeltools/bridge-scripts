
--tool:3dsmax
--name:Send to Vray(Surface)
--type:surface
new_baked_material = VRayMtl()
new_baked_material.name = ("MAT_%%MAT_NAME%%")
new_baked_material.brdf_type = 1

new_baked_material.texmap_diffuse = VRayHDRI()
new_baked_material.texmap_diffuse.HDRIMapName = ("%%ALBEDO%%")
new_baked_material.texmap_diffuse.color_space = 2

new_baked_material.Reflection = color 50 50 50
new_baked_material.reflection_lockIOR = off
new_baked_material.reflection_ior = 1.4


new_baked_material.texmap_bump = VRayNormalMap() 
normalBitmap = openBitmap "%%NORMAL%%" gamma:1.0
new_baked_material.texmap_bump.normal_map = Bitmaptexture bitmap:normalBitmap
bumpBitmap = openBitmap "%%BUMP%%" gamma:1.0
new_baked_material.texmap_bump.bump_map = Bitmaptexture bitmap:bumpBitmap
new_baked_material.texmap_bump.flip_green = on
new_baked_material.texmap_bump_multiplier = 100
	
glossBitmap = openBitmap "%%GLOSS%%" gamma:1.0
new_baked_material.texmap_reflectionGlossiness = Bitmaptexture bitmap:glossBitmap

meditMaterials[activeMeditSlot] = new_baked_material
activeMeditSlot = activeMeditSlot + 1

new_baked_material.showInViewport = true 

