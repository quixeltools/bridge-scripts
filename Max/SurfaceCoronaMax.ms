
--tool:3dsmax
--name:Send to Corona(Surface)
--type:surface
new_baked_material = CoronaMtl()
new_baked_material.name = ("MAT_%%MAT_NAME%%")
new_baked_material.bsdf = 3

new_baked_material.texmapDiffuse = CoronaMix()

diffuseBitmap = openBitmap "%%ALBEDO%%" gamma:2.2
new_baked_material.texmapDiffuse.texmapBottom = Bitmaptexture bitmap:diffuseBitmap

cavityBitmap = openBitmap "%%CAVITY%%" gamma:1
new_baked_material.texmapDiffuse.texmapTop = Bitmaptexture bitmap:cavityBitmap

new_baked_material.texmapDiffuse.mixOperation = 2
new_baked_material.texmapDiffuse.mixAmount = 0

new_baked_material.levelReflect = 1
new_baked_material.fresnelIor = 1.4
new_baked_material.pbrMode = on
new_baked_material.texmapOnDisplacement = off
new_baked_material.displacementMaximum = 10

new_baked_material.texmapBump = CoronaNormal()

normalBitmap = openBitmap "%%NORMAL%%" gamma:1.0
new_baked_material.texmapBump.NormalMap = Bitmaptexture bitmap:normalBitmap

bumpBitmap = openBitmap "%%BUMP%%" gamma:1.0
new_baked_material.texmapBump.additionalBump = Bitmaptexture bitmap:bumpBitmap

new_baked_material.texmapBump.flipred = on
new_baked_material.mapamountBump = 1
	
glossBitmap = openBitmap "%%GLOSS%%" gamma:1.0
new_baked_material.texmapReflectGlossiness = Bitmaptexture bitmap:glossBitmap

displBitmap = openBitmap "%%DISPLACEMENT%%" gamma:1.0
new_baked_material.texmapDisplace = Bitmaptexture bitmap:displBitmap

meditMaterials[activeMeditSlot] = new_baked_material
activeMeditSlot = activeMeditSlot + 1

new_baked_material.showInViewport = true