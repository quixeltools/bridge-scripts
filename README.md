# README #


### What is this repository for? ###

* A central repository for all supporting scripts and tools to use with Megascans.

### How do I get set up? ###

* The Megascans Bridge follows a specific folder structure for supported tools at the moment. Have a look at your Bridge_Data/Support/Scripts folder.
* The Megascans Bridge also runs a webserver which can be used to access scans. (http://quixel.se/megascans-bridge/BridgeAPI.pdf)

### Contribution guidelines ###

* Users can write scripts for both the Bridge and whichever 3rd party tool they are using to connect to the Bridge server.
* Ideally place comments in the scripts so other users can contribute to it with ease.
* To add any support in the Bridge drop an email at zohaib@quixel.se with the subject line [Bridge Request].

### Who do I talk to? ###

* Feel free to drop an email at support@quixel.se or zohaib@quixel.se