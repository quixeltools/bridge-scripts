--tool:marmoset
--name:Metalness roughness surface
--type:surface
@Sub SRSubdivision = SRSubdivisionFlatTessellation = 2048
@End

@Sub SRDisplacement = SRDisplacementHeight
    Displacement Map = @Tex file "%%DISPLACEMENT%%" srgb 0 filter 1 mip 0 aniso 4 wrap 1 @EndTex
    Scale = 0.1
    Scale Center = 0.5
    Relative Scale = 1
@End

@Sub SRSurface = SRSurfaceNormalMap
    Normal Map = @Tex file "%%NORMAL%%" srgb 0 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Scale & Bias = 1
    Flip X = 0
    Flip Y = 0
    Flip Z = 0
    Object Space = 0
@End

@Sub SRMicrosurface = SRMicrosurfaceGlossMap
    Gloss Map = @Tex file "%%ROUGHNESS%%" srgb 0 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel = 0
    Gloss = 1
    Horizon Smoothing = 0.5
    Invert = 1
@End

@Sub SRAlbedo = SRAlbedoMap
    Albedo Map = @Tex file "%%ALBEDO%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Color = 1 1 1
@End

@Sub SRDiffusion = SRDiffusionLambertian
@End

@Sub SRReflectivity = SRReflectivityMetalnessMap
    Metalness Map = @Tex file "%%METALNESS%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel = 0
    Metalness = 1
    Invert = 0
@End

@Sub SRReflection = SRReflectionGGX
    Horizon Occlusion = 1
@End

@Sub SROcclusion = SROcclusionMap
    Occlusion Map = @Tex file "%%AO%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel;occlusion = 0
    Occlusion = 1
    UV Set = 0
    Vertex Channel = 0
    Cavity Map = @Tex file "%%CAVITY%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel;cavity = 0
    Diffuse Cavity = 0
    Specular Cavity = 1
@End

@Sub SRMerge = SRMerge
@End