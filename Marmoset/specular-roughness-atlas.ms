--tool:marmoset
--name:Specular roughness atlas
--type:atlas
@Sub SRSubdivision = SRSubdivisionFlatTessellation = 2048
@End

@Sub SRDisplacement = SRDisplacementHeight
    Displacement Map = @Tex file "%%DISPLACEMENT%%" srgb 0 filter 1 mip 0 aniso 4 wrap 1 @EndTex
    Scale = 0.1
    Scale Center = 0.5
    Relative Scale = 1
@End

@Sub SRSurface = SRSurfaceNormalMap
    Normal Map = @Tex file "%%NORMAL%%" srgb 0 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Scale & Bias = 1
    Flip X = 0
    Flip Y = 0
    Flip Z = 0
    Object Space = 0
@End

@Sub SRMicrosurface = SRMicrosurfaceGlossMap
    Gloss Map = @Tex file "%%ROUGHNESS%%" srgb 0 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel = 0
    Gloss = 1
    Horizon Smoothing = 0.5
    Invert = 1
@End

@Sub SRAlbedo = SRAlbedoMap
    Albedo Map = @Tex file "%%ALBEDO%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Color = 1 1 1
@End

@Sub SRDiffusion = SRDiffusionSkin
    Subdermis Map = nil
    Subdermis Scatter = 0
    Subdermis Color = 0.0737534 0.34851 0.010038
    Normal Smoothing = 1
    Shadow Blur = 0.5
    Occlusion Blur = 0.5
    Translucency Map = @Tex file "%%TRANSLUCENCY%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Translucency = 1
    Translucency Color = 1 1 1
    Sky Translucency = 1
    Translucency Scatter = 0.5
    Fuzz Map = nil
    Fuzz = 0
    Fuzz Color = 1 1 1
    Fuzz Scatter = 0.5
    Fuzz Occlusion = 0.5
    Mask Fuzz with Gloss = 0
@End

@Sub SRReflectivity = SRReflectivitySpecularMap
    Specular Map = @Tex file "%%SPECULAR%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel;specular = 4
    Intensity = 1
    Color = 1 1 1
    Fresnel = 1
    Fresnel Color = 1 1 1
    Conserve Energy = 1
@End

@Sub SRReflection = SRReflectionGGX
    Horizon Occlusion = 1
@End

@Sub SROcclusion = SROcclusionMap
    Occlusion Map = @Tex file "%%AO%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel;occlusion = 0
    Occlusion = 1
    UV Set = 0
    Vertex Channel = 0
    Cavity Map = @Tex file "%%CAVITY%%" srgb 1 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel;cavity = 0
    Diffuse Cavity = 0
    Specular Cavity = 1
@End

@Sub SRTransparency = SRTransparencyAlphaTest
    Use Albedo Alpha = 0
    Alpha Map = @Tex file "%%OPACITY%%" srgb 0 filter 1 mip 1 aniso 4 wrap 1 @EndTex
    Channel = 0
    Alpha = 1
    Threshold = 0.5
@End

@Sub SRMerge = SRMerge
@End