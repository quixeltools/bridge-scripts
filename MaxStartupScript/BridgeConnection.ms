-- Startup script for 3ds max
-- Reads a file ConnectFile.txt from the AppData/LocalLow folder
-- If file exists runs the script MaxScript.cs from the same AppData/LocalLow folder
-- These files are created by the Bridge app

theTimer = dotNetObject "System.Windows.Forms.Timer"

function ReadFile = 
(
	filePath = systemTools.getEnvVariable("USERPROFILE") + "\\AppData\\LocalLow\\Quixel\\Megascans Bridge\\ConnectFile.txt"

    scriptFile = "test.txt"
	if doesFileExist(filePath) do
    (
        deleteFile filePath
        scriptFile = systemTools.getEnvVariable("USERPROFILE") + "\\AppData\\LocalLow\\Quixel\\Megascans Bridge\\MaxScript.ms"
        print "[Megascans] Load script"
    )
        
    if doesFileExist(scriptFile) do
    (
        FileIn scriptFile
    )
	
)


dotnet.addEventHandler theTimer "tick" ReadFile
theTimer.interval = 1000
theTimer.start()

