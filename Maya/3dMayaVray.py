--tool:maya
--name:Send to Maya(3d)
--type:3d

#file -import -type "%%MESH_TYPE%%"  -ignoreVersion -ra true -mergeNamespacesOnClash false -namespace "plcqZ_LOD0" -options "mo=1"  -pr "%%OBJ_PATH%%";

# Create VRayMtl and name it filePrefix_vray
newShader = cmds.shadingNode('VRayMtl', asShader=True, name="MAT_%%MAT_NAME%%")
newPlace2d = cmds.shadingNode('place2dTexture', asUtility=True, name="MAT_%%MAT_NAME%%_place2dTexture")
newSG = cmds.sets(renderable=True,noSurfaceShader=True,empty=True, name="MAT_%%MAT_NAME%%_SG")
cmds.defaultNavigation(connectToExisting=True, source=newShader, destination=newSG)

diffuseTexturePath = "%%ALBEDO%%"
if diffuseTexturePath != "":
	diffuseTexture = cmds.shadingNode('file', asTexture=True, name="Diffuse_Texture")
	cmds.setAttr(newTexture + ".filterType", 0)
	cmds.setAttr(newTexture + ".fileTextureName", diffuseTexturePath, type="string")
	cmds.defaultNavigation(connectToExisting=True, source=newPlace2d, destination=diffuseTexture)
	cmds.defaultNavigation(connectToExisting=True, source=diffuseTexture, destination=newShader + ".color")

	
displacementTexturePath = "%%DISPLACEMENT%%"
if displacementTexturePath != "":
	displacementTexture = cmds.shadingNode('file', asTexture=True, name="Displacement Texture")
	cmds.setAttr(displacementTexture + ".filterType", 0)
	cmds.setAttr(displacementTexture + ".fileTextureName", displacementTexturePath, type="string")
	cmds.defaultNavigation(connectToExisting=True, source=newPlace2d, destination=displacementTexture)
	cmds.defaultNavigation(connectToExisting=True, source=displacementTexture, destination=newShader + ".displacementShader")


rGlossTexturePath = "%%GLOSS%%"
if rGlossTexturePath != "":
	rGlossTexture = cmds.shadingNode('file', asTexture=True, name="Reflection Glossiness Texture")
	cmds.setAttr(rGlossTexture + ".filterType", 0)
	cmds.setAttr(rGlossTexture + ".fileTextureName", rGlossTexturePath, type="string")
	cmds.defaultNavigation(connectToExisting=True, source=newPlace2d, destination=rGlossTexture)
	cmds.defaultNavigation(connectToExisting=True, source=rGlossTexture + ".outColor", destination=newShader + ".reflectionGlossiness")


specularTexturePath = "%%SPECULAR%%"
if specularTexturePath != "":
	specularTexture = cmds.shadingNode('file', asTexture=True, name="Specular Texture")
	cmds.setAttr(specularTexture + ".filterType", 0)
	cmds.setAttr(specularTexture + ".fileTextureName", specularTexturePath, type="string")
	cmds.defaultNavigation(connectToExisting=True, source=newPlace2d, destination=specularTexture)
	cmds.defaultNavigation(connectToExisting=True, source=specularTexture, destination=newShader + ".reflectionColor")

normalTexturePath = "%%NORMAL%%"
if normalTexturePath != "":
	normalTexture = cmds.shadingNode('file', asTexture=True, name="Normal Texture")
	cmds.setAttr(normalTexture + ".filterType", 0)
	cmds.setAttr(normalTexture + ".fileTextureName", normalTexturePath, type="string")
	cmds.defaultNavigation(connectToExisting=True, source=newPlace2d, destination=normalTexture)
	cmds.defaultNavigation(connectToExisting=True, source=normalTexture, destination=newShader + ".bumpMap")
	cmds.setAttr(newShader + ".bumpMapType", 1)


# Print Success Message			
print("Megascans: New " + newShader + " shader created!"),