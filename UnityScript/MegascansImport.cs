﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;


//Editor script to create a material when an asset is imported to Unity 
//If sent from the Bridge app this script is automatically copied to the Unity Project by the Bridge


[InitializeOnLoad]
public class MegascansImport : AssetPostprocessor {

	static MegascansImport()
	{
		string assetPath = Application.dataPath + Path.DirectorySeparatorChar + "Resources";
		string[] allDirectories = Directory.GetDirectories(assetPath);

		foreach(string dir in allDirectories)
		{
			//assumption that Megascans directories end with _ms
			if (dir.EndsWith("_ms"))
			{
				Debug.LogError("Creating material : " + dir);
				CreateMaterial(dir);
			}
		}

	}

	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) 
	{
		foreach (var str in importedAssets)
		{
			//assumption that Megascans directories end with _ms
			if (str.EndsWith("_ms"))
			{
				CreateMaterial(str);
			}
		}
	}

	//using Unity's standard shader
	private static void CreateMaterial(string path)
	{
		if (File.Exists(path + Path.DirectorySeparatorChar + "ScanMaterial.mat"))
		{
			Debug.Log("Material already exists");
			return;
		}

		var material = new Material (Shader.Find("Standard"));

		string diffusePath = "";
		string normalPath = "";
		string occlusionPath = "";
		string specularPath = "";
		string displacementPath = "";
		string glossPath = "";

		string[] allFiles = Directory.GetFiles(path);

		foreach(string item in allFiles)
		{
			string lowerCase = item.ToLower();
			if ((lowerCase.Contains("_albedo") || lowerCase.Contains("_diffuse")) && !lowerCase.EndsWith(".meta"))
			{
				diffusePath = item;
			}

			if (lowerCase.Contains("_normal") && !lowerCase.EndsWith(".meta"))
			{
				normalPath = item;
			}

			if (lowerCase.Contains("_ao") && !lowerCase.EndsWith(".meta"))
			{
				occlusionPath = item;
			}

			if (lowerCase.Contains("_specular") && !lowerCase.EndsWith(".meta"))
			{
				specularPath = item;
			}
		
			if (lowerCase.Contains("_glossblinn") && !lowerCase.EndsWith(".meta"))
			{
				glossPath = item;
			}

			if (lowerCase.Contains("_displacement") && !lowerCase.EndsWith(".meta"))
			{
				displacementPath = item;
			}
		}


		if (diffusePath != "")
		{
			
			var diffuseTex = AssetDatabase.LoadMainAssetAtPath(diffusePath) as Texture2D;
			material.SetTexture("_MainTex" , diffuseTex);
		}

		if (normalPath != "")
		{
			material.EnableKeyword("_NORMALMAP");
			var normalTex = AssetDatabase.LoadMainAssetAtPath(normalPath) as Texture2D;
			material.SetTexture("_BumpMap" , normalTex);
		}

		if (occlusionPath != "")
		{
			var occlusionTex = AssetDatabase.LoadMainAssetAtPath(occlusionPath) as Texture2D;
			material.SetTexture("_OcclusionMap" , occlusionTex);
		}

		if (specularPath != "")
		{
			material.EnableKeyword("_SPECMAP");
			var specularTex = AssetDatabase.LoadMainAssetAtPath(specularPath) as Texture2D;
			material.SetTexture("_SpecMap" , specularTex);
		}

		if (glossPath != "")
		{
			material.EnableKeyword("_GLOSSMAP");
			var glossTex = AssetDatabase.LoadMainAssetAtPath(glossPath) as Texture2D;
			material.SetTexture("_GlossMap" , glossTex);
		}

		if (displacementPath != "")
		{
			material.EnableKeyword("_PARALLAXMAP");
			var displacementTex = AssetDatabase.LoadMainAssetAtPath(displacementPath) as Texture2D;
			material.SetTexture("_ParallaxMap" , displacementTex);
		}

		AssetDatabase.CreateAsset(material, path + Path.DirectorySeparatorChar + "ScanMaterial.mat");
	}
}
#endif
